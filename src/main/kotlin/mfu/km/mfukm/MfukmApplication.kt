package mfu.km.mfukm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MfukmApplication

fun main(args: Array<String>) {
	runApplication<MfukmApplication>(*args)
}
